#!/usr/bin/env python3
# -*- coding:utf-8 -*-

import gi

from gi.repository import Gtk

gi.require_version('Gtk', '3.0')

# clase de ventana
class Ventana():
    def __init__(self, object_name=""):
        self.builder = Gtk.Builder()
        self.builder.add_from_file("./ui/interfaz.ui")
        self.dialogo = self.builder.get_object(object_name)

        # cuando se elija un archivo
        # se creará el boton ok
        if object_name == "dlgArchivo":
            btn_aceptar = self.dialogo.add_button(Gtk.STOCK_OK, Gtk.ResponseType.OK)
            btn_aceptar.set_always_show_image(True)
            btn_aceptar.connect("clicked", self.open_click)
        if object_name == "noGuardar" or object_name == "archivoGuardar":
            btn_aceptar = self.dialogo.add_button(Gtk.STOCK_OK, Gtk.ResponseType.OK)
            btn_aceptar.set_always_show_image(True)
            btn_aceptar.connect("clicked", self.boton_ok)
        self.dialogo.show_all()

    # se regresa al inicio de la carpeta
    def open_click(self, btn=None):
        self.ruta = self.dialogo.get_current_folder()
        return self.ruta

    # funcion llamada al final de cada proceso, cuando se haga click
    def boton_ok(self, btn=None):
        self.dialogo.destroy()

