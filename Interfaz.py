#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import gi

from gi.repository import Gtk
from biopandas.pdb import PandasPdb
from dlgVentana import Ventana

gi.require_version('Gtk', '3.0')

# clase de la interfaz, la ventana, el recuadro
class Interfaz():
    def __init__(self, datos, nombre, titulo=""):
        # atributos de informacion
        self.datos = datos
        self.nombre = nombre
        self.builder = Gtk.Builder()
        self.builder.add_from_file("./ui/interfaz.ui")
        self.visualizar = self.builder.get_object("visualizarProt")
        self.visualizar.set_title("Info de Proteina")
        self.visualizar.resize(500, 400)
        self.visualizar.show_all()

        # label
        self.label_visualizar_prot = self.builder.get_object("labelvisualizarProt")

        # botones salir y guardar
        quit_boton = self.builder.get_object("btnQuit")
        quit_boton.connect("clicked", self.quit_click)
        save_boton = self.builder.get_object("btnSave")
        save_boton.connect("clicked", self.save_click)

        # parte del comboboxtext
        # lista
        # se obtiene ruta con el nombre
        lista = ["ATOM", "HETATM", "ANISOU", "OTHERS"]
        self.comboboxtext = self.builder.get_object("comboboxtext")
        self.comboboxtext.connect("changed", self.comboboxtext_changed)
        self.comboboxtext.set_entry_text_column(0)
        cont = 0
        cont2 = 0
        for index in lista:
            self.comboboxtext.append_text(index)
        for index in self.datos:
            if index == "/":
                cont += 1
            self.text = ""
        for index in self.datos:
            if cont2 == cont-1:
                break
            if index == "/":
                cont2 += 1
            self.text += index
        self.complete_datos = self.text + self.nombre

    # funcion salir del visualizador
    def quit_click(self, btn=None):
        self.visualizar.destroy()

    # funcion guardar pdb del visualizador
    # se utiliza pandas
    def save_click(self, btn=None):
        panpdb = PandasPdb()
        panpdb.read_pdb(self.datos)

    # si existe cada variable de la lista se guarada, sino, se llama a Ventana
        if self.pdb_column == "ATOM":
            if panpdb.df["ATOM"].empty is True:
                dlgVentana = Ventana(object_nombre="noGuardar")
            else:
                panpdb.to_pdb(path=self.complete_datos + "_ATOM.pdb", records=['ATOM'], gz=False, append_newline=True)
                dlgVentana = Ventana(object_nombre="archivoGuardar")

        if self.pdb_column == "HETATM":
            if panpdb.df["HETATM"].empty is True:
                dlgVentana = Ventana(object_nombre="noGuardar")
            else:
                panpdb.to_pdb(path=self.complete_datos + "_HETATM.pdb", records=['HETATM'], gz=False, append_newline=True)
                dlgVentana = Ventana(object_nombre="archivoGuardar")

        if self.pdb_column == "ANISOU":
            if panpdb.df["ANISOU"].empty is True:
                self.label_visualizar_prot.set_text("NO VAlUE")
                dlgVentana = Ventana(object_nombre="noGuardar")
            else:
                panpdb.to_pdb(path=self.complete_datos + "_ANISOU.pdb", records=["ANISOU"], gz=False, append_newline=True)
                dlgVentana = Ventana(object_nombre="archivoGuardar")

        if self.pdb_column == "OTHERS":
            if panpdb.df["OTHERS"].empty is True:
                self.label_visualizar_prot.set_text("NO VAlUE")
                dlgVentana = Ventana(object_nombre="noGuardar")
            else:
                panpdb.to_pdb(path=self.complete_datos + "_OTHERS.pdb", records=['OTHERS'], gz=False, append_newline=True)
                dlgVentana = Ventana(object_nombre="archivoGuardar")

# funcion muestreo de informacion
# se utiliza pandas
# se trae la informacion obtenida de pdb
    def comboboxtext_changed(self, cmb=None):
        panpdb = PandasPdb()
        panpdb.read_pdb(self.datos)
        self.valor_comboboxtext = self.comboboxtext.get_active_text()
        self.pdb_column = "".join([self.valor_comboboxtext])
        text = panpdb.pdb_text[:1000]

        # si existe la variable se muestra la la imfo
        # si está vacio muestra un NO VALUE
        if self.pdb_column == "ATOM":
            if panpdb.df["ATOM"].empty is True:
                self.label_visualizar_prot.set_text("NO VAlUE")
            else:
                self.label_visualizar_prot.set_text(str(panpdb.df["ATOM"].to_string()))

        if self.pdb_column == "HETATM":
            if panpdb.df["HETATM"].empty is True:
                self.label_visualizar_prot.set_text("NO VAlUE")
            else:
                self.label_visualizar_prot.set_text(str(panpdb.df["HETATM"].to_string()))

        if self.pdb_column == "ANISOU":
            if panpdb.df["ANISOU"].empty is True:
                self.label_visualizar_prot.set_text("NO VAlUE")
            else:
                self.label_visualizar_prot.set_text(str(panpdb.df["ANISOU"].to_string()))

        if self.pdb_column == "OTHERS":
            if panpdb.df["OTHERS"].empty is True:
                self.label_visualizar_prot.set_text("NO VAlUE")
            else:
                self.label_visualizar_prot.set_text(str(panpdb.df["OTHERS"].to_string()))


