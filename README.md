## Visualizador de proteinas en archivos PDB
# ******************************************

Programa que permite la visualización de proteínas en formato pdb.                                                                                                                       Este la representa de forma cartoon.                                                                                                                                                                                                                                                    También permite observar información referida a valores filtrados como "ATOM", "HETATM", "ANISOU" y "OTHERS", los cuales son propios de los archivos pdb.

# Introducción
El programa permite seleccionar un archivo pdb para poder visualizar una imagen de proteina de este, así como también, algunos de sus datos.                                                                                                       Se puede apreciar esto, en un menú que contiene tres botones, a los que se puede accerder y obtener; visualización de la proteia, dentro de este se puede ver información de la proteina y ver datos y créditos de la aplicación.

# Requisitos antes de
- Se necesita un sistema operativo linux, preferentemente ubuntu, junto con un visualizador del código.
- Instalación de: **Python3, Gtk, Biopandas, Pymol, Glade**

# Para ver información sobre esto, visitar:
* https://lazka.github.io/pgi-docs/#Gtk-3.0
* https://www.youtube.com/watch?v=2Ia7ayHNER8
* https://zoomadmin.com/HowToInstall/UbuntuPackage/pymol
* https://howtoinstall.co/es/ubuntu/xenial/glade

# Ejecución del programa
- Se debe abrir el archivo wnPrincipal y compilar para que se ejecute la ventana.

# Autor
*Franco Cifuentes Gizzi*

# Expresiones de Gratitud
* Referencia otorgada en la plataforma de GitLab de Fabio Durán Verdugo: https://gitlab.com/fabioduran/programacion-avanzada-2020/-/tree/master/unidad1/gtk

# Licencia
  *Este proyecto esta bajo la licencia GPL.3.0*