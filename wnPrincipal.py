#!/usr/bin/env python3
# -*- coding:utf-8 -*-

import os
import gi
import pymol

gi.require_version('Gtk', '3.0')
from gi.repository import Gtk
from biopandas.pdb import PandasPdb
from Interfaz import Interfaz
from dlgVentana import Ventana


# clase principal
class Principal():
    def __init__(self):
        self.builder = Gtk.Builder()
        # se busca la ventana pa visualizar
        self.builder.add_from_file("./ui/interfaz.ui")
        self.window = self.builder.get_object("wnPrincipal")
        self.window.set_title("VISUALIZADOR DE PROTEINAS PDB")
        self.window.resize(600, 400)
        self.window.connect("destroy", Gtk.main_quit)
        self.img = self.builder.get_object("openimagen")

        # __BOTONES__
        #boton abrir
        open_click = self.builder.get_object("btnOpen")
        open_click.connect("clicked", self.open_click)
        #boton de info
        info_click = self.builder.get_object("btnInfo")
        info_click.connect("clicked", self.info_click)
        # boton para ver imagen
        visualizar_click = self.builder.get_object("btnImage")
        visualizar_click.connect("clicked", self.visualizar_click)

        # label
        self.label_principal = self.builder.get_object("labelPrincipal")
        self.label_carac = self.builder.get_object("labelCarac")
        # tree
        self.tree = self.builder.get_object("tree")
        self.tree_model = Gtk.ListStore(*([str]))
        self.tree.set_model(model=self.tree_model)
        cell = Gtk.CellRendererText()
        column = Gtk.TreeViewColumn(title="Código de proteína",
                                    cell_renderer=cell,
                                    text=0)
        self.tree.append_column(column)
        self.tree.connect("cursor-changed", self.conjunto_lista)
        self.label = self.builder.get_object("label")

        # variables de ventana
        self.pdb_file = []
        self.proteinas = []
        self.direct = []
        self.visualizar = []
        self.window.show_all()

    # funcion de seleccion de proteina
    # para luego visualizar
    def conjunto_lista(self, tree=None):
        tree_model, it = self.tree.get_selection().get_selected()
        if tree_model is None or it is None:
            return
        self.visualizar.append(tree_model.get_value(it, 0))
        self.label.set_text(self.ruta+"/"+tree_model.get_value(it, 0)+".pdb")
        self.ver_proteina()

    # función limpia el conjunto creado para abrir de nuevo la carpeta
    def clear_lista(self):
        self.tree_model.clear()
        self.proteinas.clear()
        self.pdb_file.clear()

    # funcion boton de informacion
    # abre el recuadro que contiene datos de info
    def info_click(self, btn=None):
        dlgVentana = Ventana(object_name="dlgInfo")
        dlgVentana.dialogo.run()
        dlgVentana.dialogo.destroy()

    # funcion boton abrir
    # llegando a la ruta de la carpeta contenedora
    # y se crea un alista con lo encontrado (solo .pdb)
    # obteninendo asi una lista solamente con los 
    # nombres de las proteinas
    def open_click(self, btn=None):
        self.clear_lista()
        dlgVentana = Ventana(object_name="dlgArchivo")
        response = dlgVentana.dialogo.run()
        self.ruta = dlgVentana.open_click(self)
        dlgVentana.dialogo.destroy()
        self.direct = os.listdir(self.ruta)
        for i in self.direct:
            if os.path.isfile(os.path.join(self.ruta, i)) and i.endswith(".pdb"):
                self.pdb_file.append(i)
        for i in self.pdb_file:
            texto = ""
            for j in i:
                if j == ".":
                    break
                texto += j
            self.proteinas.append(texto)
        # se llama al list store para llenarlo
        self.llenar_liststore()
        dlgVentana.dialogo.destroy()

    # funcion añade las proteinas en el list store
    def llenar_liststore(self):
        for i in self.proteinas:
            self.tree_model.append([i])

    # funcion para visualizar proteina seleccionada por usuario
    def visualizar_click(self, btn=None):
        tree_model, it = self.tree.get_selection().get_selected()
        if tree_model is None or it is None:
            return
        directory = self.pdb_file
        name = self.pdb_name
        dlgVentana = Interfaz(directory, name)
    
    # funcion que guarda como archivo png
    def guarda_prot(self):
        archivo_png = self.ruta + "/" + self.visualizar[0] + ".png"
        self.img.set_from_file(archivo_png)
        self.visualizar.clear()

    # funcion muestra la proteina y crea ruta deesta en png
    def ver_proteina(self):
        self.pdb_file = self.ruta + "/" + self.visualizar[0] + ".pdb"
        self.pdb_name = self.visualizar[0]
        
        # se usa pymol para su visualizacion
        pymol.cmd.load(self.pdb_file, self.pdb_name)
        pymol.cmd.disable("all")
        pymol.cmd.enable(self.pdb_name)
        pymol.cmd.get_names()
        pymol.cmd.hide('all')
        pymol.cmd.show('cartoon')
        pymol.cmd.set('ray_opaque_background', 0)
        pymol.cmd.pretty(self.pdb_name)
        pymol.cmd.png(self.ruta + "/" + self.pdb_name + ".png")
        pymol.cmd.ray()
        # llama a funcion para guardar como png
        self.guarda_prot()
        # usar panda para imprimir los primeros 1000 caracteres
        ppdb = PandasPdb()
        ppdb.read_pdb(self.pdb_file)
        text = ppdb.pdb_text[:1000]
        self.label_principal.set_text(text)

if __name__ == "__main__":
    PRINCIPAL = Principal()
    Gtk.main()

